package net.avcompris.examples.users3.core.it;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.examples.users3.dao.impl.DbTables.AUTH;
import static net.avcompris.examples.users3.dao.impl.DbTables.USERS;

import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.ApiTestsUtils;
import net.avcompris.commons3.api.tests.TestSpecParametersExtension;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.core.tests.AbstractUsersServiceApiTest;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInRDS;
import net.avcompris.examples.users3.dao.impl.UsersDaoInRDS;

@ExtendWith(TestSpecParametersExtension.class)
public class UsersServiceApiTests extends AbstractUsersServiceApiTest {

	public UsersServiceApiTests(final TestSpec spec) {

		super(spec);
	}

	@Override
	protected Pair<UsersDao, AuthDao> getBeans(final Clock clock) throws Exception {

		final String usersTableName = ensureDbTableName(USERS);
		final String authTableName = ensureDbTableName(AUTH);

		final AuthDao authDao = new AuthDaoInRDS(getDataSource(), authTableName, clock);
		final UsersDao usersDao = new UsersDaoInRDS(getDataSource(), usersTableName, authTableName, clock);

		return Pair.of(usersDao, authDao);
	}

	public static Stream<Arguments> testSpecs() throws Exception {

		return ApiTestsUtils.testSpecs( //
				UserInfo.class, //
				UsersInfo.class);
	}
}
