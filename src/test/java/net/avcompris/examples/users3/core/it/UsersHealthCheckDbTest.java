package net.avcompris.examples.users3.core.it;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.commons3.it.utils.IntegrationTestUtils.getTestProperty;
import static net.avcompris.examples.users3.dao.impl.DbTables.AUTH;
import static net.avcompris.examples.users3.dao.impl.DbTables.CORRELATIONS;
import static net.avcompris.examples.users3.dao.impl.DbTables.USERS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.web.MutableHealthCheck;
import net.avcompris.examples.users3.dao.impl.UsersHealthCheckDb;

public class UsersHealthCheckDbTest {

	@Test
	public void testOK() throws Exception {

		ensureDbTableName(CORRELATIONS);
		ensureDbTableName(USERS);
		ensureDbTableName(AUTH);

		final MutableHealthCheck healthCheck = instantiate(MutableHealthCheck.class) //
				.setOk(true) //
				.setComponentName("Users");

		UsersHealthCheckDb.populateRuntimeDbStatus( //
				healthCheck, //
				getDataSource(), //
				getTestProperty("rds.tableNamePrefix"));

		assertTrue(healthCheck.isOk());
		assertEquals(4, healthCheck.getRuntimeDbStatus().getTables().length);
		assertNull(healthCheck.getErrors());
	}
}
