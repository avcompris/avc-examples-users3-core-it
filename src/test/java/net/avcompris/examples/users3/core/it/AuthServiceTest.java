package net.avcompris.examples.users3.core.it;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.examples.users3.dao.impl.DbTables.AUTH;
import static net.avcompris.examples.users3.dao.impl.DbTables.USERS;

import org.apache.commons.lang3.tuple.Pair;

import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.core.tests.AbstractAuthServiceTest;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInRDS;
import net.avcompris.examples.users3.dao.impl.UsersDaoInRDS;

public class AuthServiceTest extends AbstractAuthServiceTest {

	@Override
	protected Pair<UsersDao, AuthDao> getBeans(final Clock clock) throws Exception {

		final String usersTableName = ensureDbTableName(USERS);
		final String authTableName = ensureDbTableName(AUTH);

		return Pair.of( //
				new UsersDaoInRDS(getDataSource(), usersTableName, authTableName, clock), //
				new AuthDaoInRDS(getDataSource(), authTableName, clock));
	}
}
