package net.avcompris.examples.users3.core.it;

import static net.avcompris.commons3.core.it.utils.RDSTestUtils.ensureDbTableName;
import static net.avcompris.commons3.core.it.utils.RDSTestUtils.getDataSource;
import static net.avcompris.examples.users3.dao.impl.DbTables.CORRELATIONS;

import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.shared3.dao.impl.CorrelationDaoInRDS;
import net.avcompris.examples.users3.core.tests.AbstractCorrelationServiceTest;

public class CorrelationServiceTest extends AbstractCorrelationServiceTest {

	@Override
	protected CorrelationDao getBeans(final Clock clock) throws Exception {

		final String correlationsTableName = ensureDbTableName(CORRELATIONS);

		return new CorrelationDaoInRDS(getDataSource(), correlationsTableName, clock);
	}
}
